$(function(){

	var baseUrl=$("#base_url").val();
	$('.modal').modal();
	$('select').material_select();

	/*==========================Students=================================================*/

	$("#addStudent").on("click",function(){
		$("#addStudentModal").modal('open');
	});

	$("#sendStudentData").on("click",function(){
		var formdata=new FormData($("#addStudentForm")[0]);
		$.ajax({
				url:baseUrl+"Admin/addStudent/",
				data:formdata,
				type:"POST",
				processData:false,
				contentType:false,
				success:function(result){
					alert("Students Added Successfully..");
					window.location.reload();
				}
		});

	});

	$("#updateStudent").on("click",function(){
	var formdata=new FormData($("#updateStudentForm")[0]);
	$.ajax({
			url:baseUrl+"Admin/updateStudent/",
			data:formdata,
			type:"POST",
			processData:false,
			contentType:false,
			success:function(result){
				alert("Update Data Successfully..");
				window.location.reload();
			}
	});
	});

	$(".edit-btn").on("click",function(){
		$("#editStudentModal").modal('open');
		$("#editStudentModal .modal-content").html("");
		var studId=$(this).data('stud-id');
		$.post(baseUrl+"Admin/editStudent/"+studId,function(data){			
			$("#editStudentModal .modal-content").html(data);
				Materialize.updateTextFields();
				$('select').material_select();
		});

	});

	$("#updateRole").on("click",function(){
	var formdata=new FormData($("#updateRoleForm")[0]);
	$.ajax({
			url:baseUrl+"Admin/updateRole/",
			data:formdata,
			type:"POST",
			processData:false,
			contentType:false,
			success:function(result){
				//alert("Update Data Successfully..");
				//window.location.reload();
			}
	});
	});

	$(".role-edit-btn").on("click",function(){
		$("#editRoleModal").modal('open');
		$("#editRoleModal .modal-content").html("");
		var roleId=$(this).data('role-id');
		$.post(baseUrl+"Admin/editRole/"+roleId,function(data){			
			$("#editRoleModal .modal-content").html(data);
				Materialize.updateTextFields();
				$('select').material_select();
		});

	});

	/*======================User==========================================*/
		$("#addUser").on("click",function(){
		$("#addUserModal").modal('open');
	});

	$("#sendUserData").on("click",function(){
	var data={
		"role":$("#role").val(),
		"name":$("#name").val(),
		"username":$("#username").val(),
		"password":$("#password").val()		
	};
	$.post(baseUrl+"Admin/addUser/",{data:data},function(data){
		//var data=$.parseJSON(data);
		window.location.reload();

	});

	});




});