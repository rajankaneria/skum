<?php 
class User_model extends CI_Model{

    function getProfile($profileID){
        $query = $this->db->query("select * from profile where id='$profileID'");
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return [];
        }
    }

    function login($uname,$pass){
        $query = $this->db->query("
            select r.name from login l
            join role r on l.role_id = r.id
            where  l.uname='$uname' and l.pass='$pass'
            ");
        $count = $query->num_rows();
        $loginRow = $query->row_array();

        $role = $loginRow["name"];        
        if($count!=0)
        {           
            $details = array('status' => "1", 'message' => "Success", 'role' => "$role");
            $this->session->set_userdata("role",$uname);
            
            //$this->session->set_userdata("email",$data["email"]);
        }
        else
        {
            $details = array('status' => "0", 'message' => "Invalid Username and Password");
        }
        return $details;
    }

    function classdiv(){
        $query = $this->db->query("SELECT distinct class FROM profile order by class ASc");
        $result = $query->result_array();

        return $result;
    }

    function Allclassdiv(){
        $query = $this->db->query("select * from class ");
        $result = $query->result_array();

        return $result;
    }

    function attendenceInsert($s_stddiv,$s_rollno){

        $now = new DateTime();
        $todayDate = $now->format('Y-m-d');
        $query = $this->db->query("insert into attendence(s_stddiv,s_rollno,date) values('$s_stddiv','$s_rollno','$todayDate')");
        
        $details =  array('status' => "1", 'message' => "Success");
        
        return $details;
    }

    function attendenceDelete($s_stddiv,$s_rollno){
        
        $now = new DateTime();
        $todayDate = $now->format('Y-m-d');
        $query = $this->db->query("delete from attendence where s_stddiv = '$s_stddiv' AND s_rollno = '$s_rollno' AND date = '$todayDate'");
        
        $details =  array('status' => "1", 'message' => "Success");
        
        return $details;
    }


    function studentInsert($studentData){

        $query = $this->db->insert("profile",$studentData);
        $profileID = $this->db->insert_id();
        $details =  array('status' => "1", 'message' => "Success","profile_id" => $profileID);
        
        $studentID = $studentData["class"].$studentData["rollno"];
        $loginData = array(
            "uname" => $studentID,
            "pass" => $studentID,
            "role_id" => 3
        );
        $query = $this->db->insert("login",$loginData);
        return $details;
    }

    function studentUpdate($id,$studentData){
        $query = $this->db->where("id",$id);
        $query = $this->db->Update("profile",$studentData);
        $profileID = $this->db->insert_id();
        $details =  array('status' => "1", 'message' => "Success");
        
        return $details;
    }

    function noticeBoardInsert($title,$description){

        $query = $this->db->query("insert into noticeboard(title,description) values('$title','$description')");
        
        $details =  array('status' => "1", 'message' => "Success");
        
        return $details;
    }

    function noticeBoardView(){
        $query = $this->db->query("select * from noticeboard");
        $result = $query->result_array();
        
        return $result;
    }

    function noticeBoardDelete($id){
        
        $query = $this->db->query("delete from noticeboard where id = '$id'");
        
        $details =  array('status' => "1", 'message' => "Success");
        
        return $details;
    }

    function activityInsert($title,$description,$activity_date){

        $activity_date = date("Y-m-d", strtotime($activity_date));
        $query = $this->db->query("insert into activity(title,description,activity_date) values('$title','$description','$activity_date')");
        
        $details =  array('status' => "1", 'message' => "Success");
        
        return $details;
    }

    function studentFatch($class){
        $query = $this->db->query("select * from profile where class='$class'");
        $count = $query->num_rows();
        $result = $query->result_array();
        if($count!=0)
        {           
            $details = array('status' => "1", 'message' => "Success", 'Student' => $result);
        }
        else
        {
            $details = array('status' => "0", 'message' => "Student Not Found");
        }
        return $details;
    }

    function roleView(){

        $query = $this->db->query("select * from role where name != 'Student'");
        $result = $query->result_array();
        
        return $result;

    }

    function userInsert($role,$name,$username,$password){

        $query = $this->db->query("insert into user(role,name,username,password) values('$role','$name','$username','$password')");
        
        $details =  array('status' => "1", 'message' => "Success");
        
        return $details;
    }

    function roleUser($role){
        $query = $this->db->query("select * from user where role = '$role'");
        $count = $query->num_rows();
        $result = $query->result_array();
        if($count!=0)
        {           
            $details = array('status' => "1", 'message' => "Success", 'RoleUser' => $result);
        }
        else
        {
            $details = array('status' => "0", 'message' => "Role for User Not Found");
        }
        return $details;
    }

    function backendRoles(){
        $query = $this->db->query("select * from role where id != 3");
        $result = $query->result_array();
        return $result;
    }

    function allUsers(){
        $query = $this->db->query("select * from user");
        $result = $query->result_array();
        return $result;
    }

    function userEdit($id,$role){
        $query = $this->db->where("id",$id);
        $query = $this->db->Update("user",array("role"=>$role));
        
        $details =  array('status' => "1", 'message' => "Success");
        
        return $details;
    }

}
?>