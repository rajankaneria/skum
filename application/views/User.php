
<div class="container">
 <div class="row">
 <div class="card-panel">

  <div class="page-header">
      <div class="page-title">User Management</div>      
      <div class="page-button"><a class="waves-effect waves-light btn blue" id="addUser">Add</a></div> 
  </div>
   <div class="page-content">
   	<table border="1" class="responsive-table centered">
    <thead>
   		<tr>
        <th>Order</th>
        <th>Role</th>  
        <th>Name</th>
        <th>User Name</th>   
        <th>Password</th>   
       
   		</tr>
    </thead>
    <tbody>
      <?php foreach ($allUsers as $key => $allUsersRow) { ?>
      <tr id="role-id<?php echo $allUsersRow['id']; ?>">  
        <td><?php echo $allUsersRow['id']; ?></td>
        <td><?php echo $allUsersRow['role']; ?></td>
        <td><?php echo $allUsersRow['name']; ?></td>  
        <td><?php echo $allUsersRow['username']; ?></td>      
        <td><?php echo $allUsersRow['password']; ?></td>          
      </tr>
      <?php } ?>
      </tbody>
   	</table>
   </div>
  </div>
 </div>  
</div>   

  <!-- Modal Structure -->
  <div id="addUserModal" class="modal">
    <div class="modal-content">
      <?php $this->load->view("addUser",array("roleData"=>$roleData)); ?>      
    </div>
    <div class="modal-footer">
      <a id="sendUserData" href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Save</a>
    </div>
  </div>
