
<div class="container">
 <div class="row">

 <div class="card-panel">

  <div class="page-header">
      <div class="page-title">Students Management</div>      
      <div class="page-button"><a class="waves-effect waves-light btn blue" id="addStudent">Add</a></div>
  </div>
   <div class="page-content">
   	<table border="1" class="responsive-table centered">
    <thead>
   		<tr>
        <th>Order</th>
        <th>Name</th>       
        <th>Class</th> 
        <th>Roll No.</th>   
        <th>Medium</th>  
        <th>Pincode</th>  
        <th>Bod</th>
        <th>AddmissionDate</th>    
        <th>Actions</th>
   		</tr>
    </thead>
    <tbody>
      <?php foreach ($allStudentData as $key => $allStudentRow) { ?>
      <tr id="stud-id<?php echo $allStudentRow['id']; ?>">  
         <td><?php echo $allStudentRow['id']; ?></td>
        <td><?php echo $allStudentRow['name']; ?></td>         
        <td><?php echo $allStudentRow['class']; ?></td>
        <td><?php echo $allStudentRow['rollno']; ?></td>  
        <td><?php echo $allStudentRow['medium']; ?></td>
        <td><?php echo $allStudentRow['pincode']; ?></td>
        <td><?php echo $allStudentRow['bod']; ?></td>
        <td><?php echo $allStudentRow['addmissionDate']; ?></td>
           
       <!--  <td><img src="<?php echo base_url(); ?>html/images/students/<?php echo $allStudentRow['image']; ?>" width="400" height="300"/></td>   -->   
        <td class="right-align blog-btn">      
        <a data-stud-id="<?php echo $allStudentRow['id']; ?>" class="btn-floating waves-effect waves-light blue-grey edit-btn"><i class="material-icons">mode_edit</i></a>
        </td>
      </tr>
      <?php } ?>
      </tbody>
   	</table>
   </div>
  </div>
 </div>  
</div>   

<!-- Modal Structure -->
  <div id="addStudentModal" class="modal">
    <div class="modal-content">
      <?php $this->load->view("addStudents",array("allClassData"=>$allClassData)); ?>      
    </div>
    <div class="modal-footer">
      <a id="sendStudentData" href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Save</a>
    </div>
  </div>

  <!-- Modal Structure -->
   <div id="editStudentModal" class="modal">
    <div class="modal-content">
     </div>

    <div class="modal-footer">
      <a  id="updateStudent" href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Save</a>
    </div>
  </div>
