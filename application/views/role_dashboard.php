
<div class="container">
 <div class="row">
 <div class="card-panel">

  <div class="page-header">
      <div class="page-title">Role Management</div>      
      <div class="page-button"><a class="waves-effect waves-light btn blue" id="addStudent">Add</a></div>
  </div>
   <div class="page-content">
   	<table border="1" class="responsive-table centered">
    <thead>
   		<tr>
        <th>Order</th>
        <th>Name</th>        
        <th>Actions</th>
   		</tr>
    </thead>
    <tbody>
      <?php foreach ($allRoleData as $key => $allRoleRow) { ?>
      <tr id="role-id<?php echo $allStudentRow['id']; ?>">  
        <td><?php echo $allRoleRow['id']; ?></td>
        <td><?php echo $allRoleRow['name']; ?></td>     
          
        <td class="right-align blog-btn">      
        <a data-role-id="<?php echo $allRoleRow['id']; ?>" class="btn-floating waves-effect waves-light blue-grey edit-btn"><i class="material-icons">mode_edit</i></a>
        </td>
      </tr>
      <?php } ?>
      </tbody>
   	</table>
   </div>
  </div>
 </div>  
</div>   

  <!-- Modal Structure -->
   <div id="editStudentModal" class="modal">
    <div class="modal-content">
     </div>

    <div class="modal-footer">
      <a  id="updateStudent" href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Save</a>
    </div>
  </div>
