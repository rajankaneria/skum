<div class="main-content-area-login">
	<div class="row">
		<div class="skum-logo-area">
			<img src="<?php echo base_url(); ?>html/images/logo.png">
		</div>
		<div class="languages-btns-area">
			<p>Select Language</p>
			<div class="language-select">
				<div class="language-item first selected">English</div>
				<div class="language-item ">Gujarati</div>
				<div class="language-item last">Hindi</div>
			</div>
		</div>
		<div class="login-form-area">
			<form id="login_form" method="post">
				<div class="row">
					<div class="input-field col s12">
			          <input id="uname" type="text" name="uname" class="validate">
			          <label for="uname">Enter UserName</label>
			        </div>
				</div>
				<div class="row">
					<div class="input-field col s12">
			          <input id="pass" type="password" name="pass" class="validate">
			          <label for="pass">Enter Passwords</label>
			        </div>
				</div>
				<div class="row">
					<div class="input-field col s12">
			          <input id="login" type="button" value="Login" class="waves-effect waves-light btn lng-btn">
			        </div>
			        <!-- <div class="input-field col s12 m6">
			          <a href="#">Forgot Password?</a>
			        </div> -->
				</div>
			</form>	
		</div>
	</div>
</div>