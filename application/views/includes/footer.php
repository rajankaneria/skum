	 	<footer class="page-footer z-depth-1">
          <div class="row ftr-area">
            <div class="col s12 m4">
              © 2017 <span class="white-text"> SKUM School</span> | All Rights Reserved.
            </div>
            <div class="col s12 m4 socials-links">  
              <a class="footer-link-right" href="#!">
                <i class="fa fa-facebook-official" aria-hidden="true"></i>
              </a>
              <a class="footer-link-right" href="#!">
                <i class="fa fa-twitter" aria-hidden="true"></i>
              </a> 
              <a class="footer-link-right" href="#!">
                <i class="fa fa-instagram" aria-hidden="true"></i>
              </a> 
              <a class="footer-link-right" href="#!">
                <i class="fa fa-youtube-play" aria-hidden="true"></i>
              </a> 
            </div>
            <div class="col s12 m4">  
                Developed by <a href="http://intelliworkz.com/" target="_page">Intelliworkz.com</a>
            </div>
          </div>
    </footer>

	 	<input type="hidden" id="site_url" value="<?php echo site_url(); ?>" />
	  	<input type="hidden" id="base_url" value="<?php echo base_url(); ?>" />
	  	<!--  Scripts-->
	  	<?php foreach($jsFiles as $fileName){ ?>
	  	<script src="<?php echo base_url(); ?>html/js/<?php echo $fileName; ?>"></script>
	  	<?php } ?>
	</body>
</html>