<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <title>SKUM - <?php echo $pageTitle; ?></title>
    <!-- CSS-->

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/css/materialize.min.css">

    <link href="<?php echo base_url(); ?>html/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>html/css/mobile_responsive.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Inconsolata|Raleway" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/js/materialize.min.js"></script>
     
    <script src="<?php echo base_url(); ?>html/js/script.js"></script>
    <?php foreach($stylesheet as $fileName){ ?>
    <link href="<?php echo base_url(); ?>html/css/<?php echo $fileName; ?>" rel="stylesheet">
    <?php } ?>
    <script src="https://use.fontawesome.com/4c9f41dc36.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <script>
        $(document).ready(function (){
            $(".click").click(function (){
                $('html, body').animate({
                    scrollTop: $("#our_partner").offset().top
                }, 2000);
            });
        });
    </script>

</head>
<body>
<style type="text/css">
    .me{background: red;}
</style>
<nav class="nav-bar" role="navigation" id="myHeader">
    <div class="nav-wrapper container">
      <a id="logo-container" href="<?php echo base_url(); ?>home" class="brand-logo main-logo"><img src="<?php echo base_url(); ?>html/images/logo.png" alt="SKUM"/></a>
      <ul class="desktop-nav-link right hide-on-med-and-down">
        <li><a href="<?php echo base_url(); ?>home" class="<?php if($this->uri->segment(1)=="home") { ?> activeMenu <?php } ?>">Home</a></li>
        <ul id="dropdown1" class="dropdown-content dropdown-about">
          <li><a href="<?php echo base_url(); ?>about/skum" class="<?php if($this->uri->segment(1)=="about") { ?> activeMenu <?php } ?>">
            SKUM SCHOOL
          </a></li>
          <li><a href="<?php echo base_url(); ?>about/girlsHostel" class="<?php if($this->uri->segment(1)=="about") { ?> activeMenu <?php } ?>">
            GIRL’S HOSTEL BUILDING 
          </a></li>
          <li><a href="<?php echo base_url(); ?>about/nursingHostel" class="<?php if($this->uri->segment(1)=="about") { ?> activeMenu <?php } ?>">
            NURSING COLLEGE BUILDING
          </a></li>
        </ul>
        <li><a class="dropdown-button" href="#!" data-activates="dropdown1">About<i class="material-icons right">arrow_drop_down</i></a></li>
        <li><a href="<?php echo base_url(); ?>team" class="<?php if($this->uri->segment(1)=="Team") { ?> activeMenu <?php } ?>">Team</a></li>
        <!-- <li><a href="<?php echo base_url(); ?>institute" class="<?php if($this->uri->segment(1)=="institute") { ?> activeMenu <?php } ?>">Institute</a></li> -->
        <li><a href="<?php echo base_url(); ?>activity" class="<?php if($this->uri->segment(1)=="activity") { ?> activeMenu <?php } ?>">Activity</a></li>
        <!--<li><a href="Event" class="<?php if($this->uri->segment(1)=="event") { ?> activeMenu <?php } ?>">Event</a></li>-->
        <li><a href="<?php echo base_url();?>mission_eklavya" class="<?php if($this->uri->segment(1)=="mission_eklavya") { ?> activeMenu <?php } ?>">Mission Eklavya</a></li>
        <li><a href="contact" class="<?php if($this->uri->segment(1)=="contact") { ?> activeMenu <?php } ?>">Contact us</a></li>
      </ul>
        
      <ul id="nav-mobile" class="side-nav">
        <li><a href="<?php echo base_url(); ?>home" class="<?php if($this->uri->segment(1)=="home") { ?> activeMenu <?php } ?>">Home</a></li>
        <li>
            <ul class="collapsible collapsible-accordion">
              <li>
                <a class="collapsible-header" style="padding-left: 30px;">About<i class="material-icons white-text right">arrow_drop_down</i></a>
                <div class="collapsible-body">
                  <ul class="mobile-dropdown-area">
                    <li><a href="<?php echo base_url(); ?>about/skum" class="<?php if($this->uri->segment(1)=="about") { ?> activeMenu <?php } ?>">SKUM SCHOOL</a>
                    </li>
                    <li><a href="<?php echo base_url(); ?>about/girlsHostel" class="<?php if($this->uri->segment(1)=="about") { ?> activeMenu <?php } ?>">GIRL’S HOSTEL BUILDING </a>
                    </li>
                    <li><a href="<?php echo base_url(); ?>about/nursingHostel" class="<?php if($this->uri->segment(1)=="about") { ?> activeMenu <?php } ?>">NURSING COLLEGE BUILDING</a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
        </li>
<!-- 
        <li><a href="<?php echo base_url(); ?>about" class="<?php if($this->uri->segment(1)=="about") { ?> activeMenu <?php } ?>">About</a></li> -->
        <li><a href="<?php echo base_url(); ?>team" class="<?php if($this->uri->segment(1)=="pillars") { ?> activeMenu <?php } ?>">Team</a></li>
        <!-- <li><a href="<?php echo base_url(); ?>institute" class="<?php if($this->uri->segment(1)=="institute") { ?> activeMenu <?php } ?>">Institute</a></li> -->
        <li><a href="<?php echo base_url(); ?>activity" class="<?php if($this->uri->segment(1)=="activity") { ?> activeMenu <?php } ?>">Activity</a></li>
        <!--<li><a href="Event" class="<?php if($this->uri->segment(1)=="event") { ?> activeMenu <?php } ?>">Event</a></li> -->
        <li><a href="<?php echo base_url();?>mission_eklavya" class="<?php if($this->uri->segment(1)=="mission_eklavya") { ?> activeMenu <?php } ?>">Mission Eklavya</a></li>
        <li><a href="contact" class="<?php if($this->uri->segment(1)=="contact") { ?> activeMenu <?php } ?>">Contact us</a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
  </nav>