<div class="row">
    <form id="addStudentForm" method="post" enctype="multipart/form-data">
      <div class="row">        
        <div class="input-field col s12">
        <input id="name" name="name" type="text" class="validate" placeholder="Enter your Name">
          <label for="name">Name</label>
        </div>
      </div>

      <div class="row">        
        <div class="input-field col s12">
        <input id="pic" name="pic" type="file" class="validate" placeholder="Browse your Photo">
          <label for="name">Photo</label>
        </div>
      </div>

      <div class="row">
      <div class="input-field col s12 m12">       
       <select name="class" id="class">
        <option>Select Standard</option> 
        <?php foreach($allClassData as $key => $allClassRow){?>       
        <option value="<?php echo $allClassRow['std'].$allClassRow['div']; ?>"><?php echo $allClassRow['std'].$allClassRow['div']; ?></option>
        <?php } ?>           
        </select>     
      </div>
    </div>
      <div class="row">        
        <div class="input-field col s12">
        <input id="rollno" name="rollno" type="text" class="validate" placeholder="Enter your Roll No">
          <label for="rollno">Roll No.</label>
        </div>
      </div>
    
    <div class="row">
      <div class="input-field col s12 m12">       
       <select name="medium" id="medium">
        <option>Select Medium</option>        
        <option value="GUJ">Gujarati</option>
        <option value="ENG">English</option>              
        </select>     
      </div>
    </div>   

      <div class="row">
        <div class="input-field col s12">
          <textarea  class="validate materialize-textarea"  id="address" name="address">
              </textarea>
              <label for="address">Address</label>
        </div>
      </div>
      <div class="row">        
      <div class="input-field col s12">
        <input id="pincode" name="pincode" type="text" class="validate" placeholder="Enter your Pin Code">
          <label for="pincode">Pin Code</label>
        </div>
      </div> 

     <div class="row">        
      <div class="input-field col s12">
        <input id="bod" name="bod" type="text" class="validate" placeholder="Enter your Birth Date">
          <label for="bod">Birth Date</label>
        </div>
      </div> 

     <div class="row">        
      <div class="input-field col s12">
        <input id="addmissionDate" name="addmissionDate" type="text" class="validate" placeholder="Enter your Pin Code">
          <label for="addmissionDate">Admission Date</label>
        </div>
      </div> 
      <div class="row">        
      <div class="input-field col s12">
        <input id="gr_no" name="gr_no" type="text" class="validate" placeholder="Enter your GR No.">
          <label for="gr_no">G.R. No.</label>
        </div>
      </div> 
      <div class="row">        
      <div class="input-field col s12">
        <input id="van_no" name="van_no" type="text" class="validate" placeholder="Enter your Van No">
          <label for="van_no">Van No</label>
        </div>
      </div> 

      <div class="row">        
      <div class="input-field col s12">
        <input id="re_mobile" name="re_mobile" type="text" class="validate" placeholder="Enter your Recedencial No">
          <label for="re_mobile">Recedencial No</label>
        </div>
      </div> 

      <div class="row">        
      <div class="input-field col s12">
        <input id="of_mobile" name="of_mobile" type="text" class="validate" placeholder="Enter your Office No">
          <label for="of_mobile">Office No</label>
        </div>
      </div>  

      <div class="row">        
      <div class="input-field col s12">
        <input id="f_name" name="f_name" type="text" class="validate" placeholder="Enter your Father Name">
          <label for="f_name">Father Name</label>
        </div>
      </div>

     <div class="row">        
      <div class="input-field col s12">
        <input id="f_occupation" name="f_occupation" type="text" class="validate" placeholder="Enter your Father Occupation">
          <label for="f_occupation">Father Occupation</label>
        </div>
      </div> 
      <div class="row">        
      <div class="input-field col s12">
        <input id="f_mobile" name="f_mobile" type="text" class="validate" placeholder="Enter your GR No.">
          <label for="f_mobile">Farher Mobile No.</label>
        </div>
      </div> 

      <div class="row">        
      <div class="input-field col s12">
        <input id="f_emailid" name="f_emailid" type="text" class="validate" placeholder="Enter your Father Email Id">
          <label for="f_emailid">FarherEmail Id</label>
        </div>
      </div>

      <div class="row">        
      <div class="input-field col s12">
        <input id="m_name" name="m_name" type="text" class="validate" placeholder="Enter your Name">
          <label for="m_name">Mother Name</label>
        </div>
      </div> 
    <div class="row">        
      <div class="input-field col s12">
        <input id="m_occupation" name="m_occupation" type="text" class="validate" placeholder="Enter your GR No.">
          <label for="title">Mother Occupation</label>
        </div>
      </div> 

        <div class="row">        
      <div class="input-field col s12">
        <input id="m_mobile" name="m_mobile" type="text" class="validate" placeholder="Enter your Mother Mobile No.">
          <label for="m_mobile">Mother Mobile No.</label>
        </div>
      </div> 
      <div class="row">        
      <div class="input-field col s12">
        <input id="m_emailid" name="m_emailid" type="text" class="validate" placeholder="Enter Mother Email Id">
          <label for="m_emailid">Mother Email Id</label>
        </div>
      </div> 
    </form>
  </div>