<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webservices  extends CI_Controller {

	
	public function activity(){

		$data_back = json_decode(file_get_contents('php://input'));	
		if(isset($data_back -> {"month"}))
		{
			$month=$data_back->{"month"};
			//$month = $_POST["month"];
			$this->load->model("activity_model");
			$output = $this->activity_model->getMonthActivity($month);
		}else{
			$output = array('status' => "0",'message' => "Parameter Missing");
		}
		echo json_encode($output);
		
	}

	public function activityInsert()
	{
		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");

		if( isset($data_back->{"title"}) && isset($data_back->{"description"})&& isset($data_back->{"activity_date"}))
		{
			if( !empty($data_back->{"title"}) && !empty($data_back->{"description"}) && !empty($data_back->{"activity_date"}))
			{
				$title = $data_back -> {"title"};
				$description = $data_back -> {"description"};
				$activity_date = $data_back -> {"activity_date"};

				$details = $this->user_model->activityInsert($title,$description,$activity_date);	
			}
			else
			{
				$details = array('status' => "0", 'message' => "Parameter is Empty");
			}
		}
		else
		{
			$details = array('status' => "0",'message' => "Parameter Missing");
		}
		echo json_encode($details);
	}

	public function profile(){
		$data_back = json_decode(file_get_contents('php://input'));

		if(isset($data_back -> {"profile_id"}))
		{
			$profileID=$data_back->{"profile_id"};
			$this->load->model("user_model");
			$profile = $this->user_model->getProfile($profileID);
			if(sizeof($profile) == 0){
				$details = array('status'=>"0", 'message'=>"Profile Not Found");
			}else{
				$details = array('status'=>"1", 'message'=>"Success", 'profile' => $profile);
			}
		}else{
			$details = array('status' => "0",'message' => "Parameter Missing");
		}
		echo json_encode($details);
	}
	
	public function login(){
		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");

		if(isset($data_back->{"uname"}) && isset($data_back->{"pass"}))
		{
			if(!empty($data_back->{"uname"}) && !empty($data_back->{"pass"}))
			{
				$uname = $data_back -> {"uname"};
				$pass = $data_back -> {"pass"};
				// modal call
				$details = $this->user_model->login($uname,$pass);			
			}
			else
			{
				$details = array('status' => "0", 'message' => "Parameter is Empty");
			}
		}
		else
		{
			$details = array('status' => "0",'message' => "Parameter Missing");
		}
		echo json_encode($details);
	}

	public function classdiv(){
		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");
		$class = $this->user_model->classdiv();
		if(sizeof($class) == 0)
		{
			$details = array('status'=>"0", 'message'=>"Class Not Found");
		}
		else
		{
			$details = array('status' => "1", 'message' => "Success", 'class' => $class);
		}
		echo json_encode($details);
	}

	public function Allclassdiv(){
		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");
		$class = $this->user_model->Allclassdiv();
		if(sizeof($class) == 0)
		{
			$details = array('status'=>"0", 'message'=>"Class Not Found");
		}
		else
		{
			$details = array('status' => "1", 'message' => "Success", 'class' => $class);
		}
		echo json_encode($details);
	}

	public function attendenceInsert(){
		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");

		if(isset($data_back->{"s_stddiv"}) && isset($data_back->{"s_rollno"}))
		{
			if(!empty($data_back->{"s_stddiv"}) && !empty($data_back->{"s_rollno"}))
			{
				$s_stddiv = $data_back -> {"s_stddiv"};
				$s_rollno = $data_back -> {"s_rollno"};
				// modal call
				$details = $this->user_model->attendenceInsert($s_stddiv,$s_rollno);			
			}
			else
			{
				$details = array('status' => "0", 'message' => "Parameter is Empty");
			}
		}
		else
		{
			$details = array('status' => "0",'message' => "Parameter Missing");
		}
		echo json_encode($details);
	}

	public function attendenceDelete(){
		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");

		if(isset($data_back->{"s_stddiv"}) && isset($data_back->{"s_rollno"}))
		{
			if(!empty($data_back->{"s_stddiv"}) && !empty($data_back->{"s_rollno"}))
			{
				$s_stddiv = $data_back -> {"s_stddiv"};
				$s_rollno = $data_back -> {"s_rollno"};
				// modal call
				$details = $this->user_model->attendenceDelete($s_stddiv,$s_rollno);			
			}
			else
			{
				$details = array('status' => "0", 'message' => "Parameter is Empty");
			}
		}
		else
		{
			$details = array('status' => "0",'message' => "Parameter Missing");
		}
		echo json_encode($details);
	}

	public function studentInsert(){

		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");

		if( isset($data_back->{"name"}) &&
			isset($data_back->{"class"}) &&
			isset($data_back->{"rollno"}) &&
			isset($data_back->{"medium"}) &&
			isset($data_back->{"address"}) &&
			isset($data_back->{"pincode"}) &&
			isset($data_back->{"bod"}) &&
			isset($data_back->{"addmissionDate"}) &&
			isset($data_back->{"gr_no"}) &&
			isset($data_back->{"van_no"}) &&
			isset($data_back->{"re_mobile"}) &&
			isset($data_back->{"of_mobile"}) &&
			isset($data_back->{"f_name"}) &&
			isset($data_back->{"f_occupation"}) &&
			isset($data_back->{"f_mobile"}) &&
			isset($data_back->{"f_emailid"}) &&
			isset($data_back->{"m_name"}) &&
			isset($data_back->{"m_occupation"}) &&
			isset($data_back->{"m_mobile"}) &&
			isset($data_back->{"m_emailid"}))
		{
			if( !empty($data_back->{"name"}) &&
				!empty($data_back->{"class"}) &&
				!empty($data_back->{"rollno"}) &&
				!empty($data_back->{"medium"}) &&
				!empty($data_back->{"address"}) &&
				!empty($data_back->{"pincode"}) &&
				!empty($data_back->{"bod"}) &&
				!empty($data_back->{"addmissionDate"}) &&
				!empty($data_back->{"gr_no"}) &&
				!empty($data_back->{"van_no"}) &&
				!empty($data_back->{"f_name"}) &&
				!empty($data_back->{"f_occupation"}) &&
				!empty($data_back->{"f_mobile"}) &&
				!empty($data_back->{"f_emailid"}) &&
				!empty($data_back->{"m_name"}) &&
				!empty($data_back->{"m_occupation"}) &&
				!empty($data_back->{"m_mobile"}))
			{

				$studentData = array(
					"name" => $data_back->{"name"},
					"class" => $data_back->{"class"},
					"rollno" => $data_back->{"rollno"},
					"medium" => $data_back->{"medium"},
					"address" => $data_back->{"address"},
					"pincode" => $data_back->{"pincode"},
					"bod" => $data_back->{"bod"},
					"addmissionDate" => $data_back->{"addmissionDate"},
					"gr_no" => $data_back->{"gr_no"},
					"van_no" => $data_back->{"van_no"},
					"re_mobile" => $data_back->{"re_mobile"},
					"of_mobile" => $data_back->{"of_mobile"},
					"f_name" => $data_back->{"f_name"},
					"f_occupation" => $data_back->{"f_occupation"},
					"f_mobile" => $data_back->{"f_mobile"},
					"f_emailid" => $data_back->{"f_emailid"},
					"m_name" => $data_back->{"m_name"},
					"m_occupation" => $data_back->{"m_occupation"},
					"m_mobile" => $data_back->{"m_mobile"},
					"m_emailid" => $data_back->{"m_emailid"},
				);

				$details = $this->user_model->studentInsert($studentData);
			}
			else
			{
				$details = array('status' => "0", 'message' => "Parameter is Empty");
			}
		}
		else
		{
			$details = array('status' => "0",'message' => "Parameter Missing");
		}
		echo json_encode($details);
	}

	public function studentUpdate(){

		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");

		if( isset($data_back->{"id"}) && 
			isset($data_back->{"name"}) &&
			isset($data_back->{"class"}) &&
			isset($data_back->{"rollno"}) &&
			isset($data_back->{"medium"}) &&
			isset($data_back->{"address"}) &&
			isset($data_back->{"pincode"}) &&
			isset($data_back->{"bod"}) &&
			isset($data_back->{"addmissionDate"}) &&
			isset($data_back->{"gr_no"}) &&
			isset($data_back->{"van_no"}) &&
			isset($data_back->{"re_mobile"}) &&
			isset($data_back->{"of_mobile"}) &&
			isset($data_back->{"f_name"}) &&
			isset($data_back->{"f_occupation"}) &&
			isset($data_back->{"f_mobile"}) &&
			isset($data_back->{"f_emailid"}) &&
			isset($data_back->{"m_name"}) &&
			isset($data_back->{"m_occupation"}) &&
			isset($data_back->{"m_mobile"}) &&
			isset($data_back->{"m_emailid"}))
		{
			if( !empty($data_back->{"id"}) && 
				!empty($data_back->{"name"}) &&
				!empty($data_back->{"class"}) &&
				!empty($data_back->{"rollno"}) &&
				!empty($data_back->{"medium"}) &&
				!empty($data_back->{"address"}) &&
				!empty($data_back->{"pincode"}) &&
				!empty($data_back->{"bod"}) &&
				!empty($data_back->{"addmissionDate"}) &&
				!empty($data_back->{"gr_no"}) &&
				!empty($data_back->{"van_no"}) &&
				!empty($data_back->{"f_name"}) &&
				!empty($data_back->{"f_occupation"}) &&
				!empty($data_back->{"f_mobile"}) &&
				!empty($data_back->{"f_emailid"}) &&
				!empty($data_back->{"m_name"}) &&
				!empty($data_back->{"m_occupation"}) &&
				!empty($data_back->{"m_mobile"}))
			{
				$id = $data_back -> {"id"};
				$studentData = array(
					"name" => $data_back->{"name"},
					"class" => $data_back->{"class"},
					"rollno" => $data_back->{"rollno"},
					"medium" => $data_back->{"medium"},
					"address" => $data_back->{"address"},
					"pincode" => $data_back->{"pincode"},
					"bod" => $data_back->{"bod"},
					"addmissionDate" => $data_back->{"addmissionDate"},
					"gr_no" => $data_back->{"gr_no"},
					"van_no" => $data_back->{"van_no"},
					"re_mobile" => $data_back->{"re_mobile"},
					"of_mobile" => $data_back->{"of_mobile"},
					"f_name" => $data_back->{"f_name"},
					"f_occupation" => $data_back->{"f_occupation"},
					"f_mobile" => $data_back->{"f_mobile"},
					"f_emailid" => $data_back->{"f_emailid"},
					"m_name" => $data_back->{"m_name"},
					"m_occupation" => $data_back->{"m_occupation"},
					"m_mobile" => $data_back->{"m_mobile"},
					"m_emailid" => $data_back->{"m_emailid"},
				);

				$details = $this->user_model->studentUpdate($id,$studentData);
			}
			else
			{
				$details = array('status' => "0", 'message' => "Parameter is Empty");
			}
		}
		else
		{
			$details = array('status' => "0",'message' => "Parameter Missing");
		}
		echo json_encode($details);
	}

	public function noticeBoardInsert()
	{
		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");

		if( isset($data_back->{"title"}) && isset($data_back->{"description"}))
		{
			if( !empty($data_back->{"title"}) && !empty($data_back->{"description"}))
			{
				$title = $data_back -> {"title"};
				$description = $data_back -> {"description"};

				$details = $this->user_model->noticeBoardInsert($title,$description);	
			}
			else
			{
				$details = array('status' => "0", 'message' => "Parameter is Empty");
			}
		}
		else
		{
			$details = array('status' => "0",'message' => "Parameter Missing");
		}
		echo json_encode($details);
	}

	public function noticeBoardView(){
		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");

		$noticeBoard = $this->user_model->noticeBoardView();
		if(sizeof($noticeBoard) == 0){
			$details = array('status'=>"0", 'message'=>"Profile Not Found");
		}else{
			$details = array('status'=>"1", 'message'=>"Success", 'noticeBoard' => $noticeBoard);
		}
		echo json_encode($details);
	}

	public function noticeBoardDelete()
	{
		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");

		if( isset($data_back->{"id"}))
		{
			if( !empty($data_back->{"id"}))
			{
				$id = $data_back -> {"id"};

				$details = $this->user_model->noticeBoardDelete($id);	
			}
			else
			{
				$details = array('status' => "0", 'message' => "Parameter is Empty");
			}
		}
		else
		{
			$details = array('status' => "0",'message' => "Parameter Missing");
		}
		echo json_encode($details);
	}

	public function studentFatch(){

		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");

		if( isset($data_back->{"class"}))
		{
			if( !empty($data_back->{"class"}))
			{
				$class = $data_back -> {"class"};

				$details = $this->user_model->studentFatch($class);	
			}
			else
			{
				$details = array('status' => "0", 'message' => "Parameter is Empty");
			}
		}
		else
		{
			$details = array('status' => "0",'message' => "Parameter Missing");
		}
		echo json_encode($details);

	}

	public function roleView(){
		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");

		$role = $this->user_model->roleView();
		if(sizeof($role) == 0){
			$details = array('status'=>"0", 'message'=>"Role Not Found");
		}else{
			$details = array('status'=>"1", 'message'=>"Success", 'role' => $role);
		}
		echo json_encode($details);
	}

	public function userInsert()
	{
		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");

		if( isset($data_back->{"role"}) && isset($data_back->{"name"}) && isset($data_back->{"username"}) && isset($data_back->{"password"}))
		{
			if( !empty($data_back->{"role"}) && !empty($data_back->{"name"}) && !empty($data_back->{"username"}) && !empty($data_back->{"password"}))
			{
				$role = $data_back -> {"role"};
				$name = $data_back -> {"name"};
				$username = $data_back -> {"username"};
				$password = $data_back -> {"password"};

				$details = $this->user_model->userInsert($role,$name,$username,$password);	
			}
			else
			{
				$details = array('status' => "0", 'message' => "Parameter is Empty");
			}
		}
		else
		{
			$details = array('status' => "0",'message' => "Parameter Missing");
		}
		echo json_encode($details);
	}

	public function roleUser(){

		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");

		if( isset($data_back->{"role"}))
		{
			if(!empty($data_back->{"role"}))
			{
				$role = $data_back -> {"role"};

				$details = $this->user_model->roleUser($role);
			}
			else
			{
				$details = array('status' => "0", 'message' => "Parameter is Empty");
			}
		}
		else
		{
			$details = array('status' => "0",'message' => "Parameter Missing");
		}
		echo json_encode($details);

	}

	public function userEdit()
	{
		$data_back = json_decode(file_get_contents('php://input'));
		$this->load->model("user_model");

		if( isset($data_back->{"role"}) && isset($data_back->{"id"}))
		{
			if( !empty($data_back->{"role"}) && !empty($data_back->{"id"}))
			{
				$id = $data_back -> {"id"};
				$role = $data_back -> {"role"};

				$details = $this->user_model->userEdit($id,$role);	
			}
			else
			{
				$details = array('status' => "0", 'message' => "Parameter is Empty");
			}
		}
		else
		{
			$details = array('status' => "0",'message' => "Parameter Missing");
		}
		echo json_encode($details);
	}

}
