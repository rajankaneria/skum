<?php 
/**
* 
*/
class Admin extends CI_Controller
{

	public function studentDashboard(){
		$this->load->model("studentProfile_model");
		$allStudentData=$this->studentProfile_model->allStudent();
		$allClassData=$this->studentProfile_model->allClassData();

		$headerData = array(
			"pageTitle" => "Students",
			"stylesheet" => array("admin.css","header.css")
		);
		$footerData = array(
			"jsFiles" => array("admin.js")
		);
		$viewData = array(
			"viewName" => "student-dashboard",
            "viewData" => array("allStudentData"=>$allStudentData,"allClassData"=>$allClassData),
			"headerData" => $headerData,
			"footerData" => $footerData	
		);
		$this->load->view('admin_template',$viewData);
	
	}

	public function addStudent(){
		$this->load->model("studentProfile_model");

		$result = array();
		foreach($_POST as $key=>$value){
			$result[$key] = $value;
		}

		//var_dump($result);
		$studId=$this->studentProfile_model->addStudent($result);

		$image=$studId."_studentPhoto.".pathinfo($_FILES['pic']['name'],PATHINFO_EXTENSION);
		$updateData=array("pic"=>$image);

		$this->studentProfile_model->updateStudent($updateData,$studId);

		$config['upload_path'] = 'C:\wamp\www\skum\html\images\students';
	    $config['allowed_types'] = 'gif|jpg|png';
	    $config['overwrite'] = TRUE;
	    $config['encrypt_name'] = FALSE;
	    $config['remove_spaces'] = TRUE;
	    
	    //set name in the config file for the feature image
	    $config['file_name'] = $studId."_studentPhoto";
	    $this->load->library('upload', $config);
	    $this->upload->do_upload('pic');
	}

	public function updateStudent(){
		$this->load->model("studentProfile_model");
		$studId=$_POST["stud_id"];
		$result = array();
		foreach($_POST as $key=>$value){
			$result[$key] = $value;
		}
		unset($result['stud_id']);
		

		//var_dump($result);
		$image=$studId."_studentPhoto.".pathinfo($_FILES['pic']['name'],PATHINFO_EXTENSION);
	
		if($_FILES['pic']['name']!=""){
		$result['pic']=$image;
		$config['upload_path'] = 'C:\wamp\www\skum\html\images\students';
	    $config['allowed_types'] = 'gif|jpg|png';
	    $config['overwrite'] = TRUE;
	    $config['encrypt_name'] = FALSE;
	    $config['remove_spaces'] = TRUE;
	    
	    //set name in the config file for the feature image
	    $config['file_name'] = $studId."_studentPhoto";
	    $this->load->library('upload', $config);
	    $this->upload->do_upload('pic');
	}
	$this->studentProfile_model->updateStudent($result,$studId);

	}
	public function editStudent($studId){
		$this->load->model("studentProfile_model");
		$allClassData=$this->studentProfile_model->allClassData();
		$output=$this->studentProfile_model->editStudent($studId);
		$output["allClassData"] = $allClassData;
		$this->load->view("updateStudent",$output);
	}

	public function roleDashboard(){
	$this->load->model("role_model");
	$allRoleData=$this->role_model->allRoleData();
	$headerData = array(
		"pageTitle" => "Role",
		"stylesheet" => array("admin.css","header.css")
	);
	$footerData = array(
		"jsFiles" => array("admin.js")
	);
	$viewData = array(
		"viewName" => "roleDashboard",
        "viewData" => array("allRoleData"=>$allRoleData),
		"headerData" => $headerData,
		"footerData" => $footerData	
	);
		$this->load->view('admin_template',$viewData);	
	}

	public function updateRole(){
		$roleId=$_POST['role_id'];
		$result=array(
			"name"=>$_POST['name']
		);
		$this->load->model("role_model");
		$this->role_model->updateRole($result,$roleId);
	}

	public function editRole($roleId){
		$this->load->model("role_model");
		$allRoleData=$this->role_model->allRoleData();
		$output=$this->role_model->editRole($roleId);
		//$output["allRoleData"] = $allRoleData;
		$this->load->view("updateRole",array("allRoleData"=>$allRoleData,"output"=>$output));
	}

	public function user(){
		$this->load->model("user_model");
		$roleData=$this->user_model->backendRoles();
		$allUsers=$this->user_model->allUsers();		

		
		$headerData = array(
			"pageTitle" => "User",
			"stylesheet" => array("admin.css","header.css")
		);
		
		$footerData = array(
			"jsFiles" => array("admin.js")
		);
		$viewData = array(
			"viewName" => "user",
            "viewData" => array("roleData"=>$roleData,"allUsers"=>$allUsers),
			"headerData" => $headerData,
			"footerData" => $footerData	
		);
		$this->load->view('admin_template',$viewData);
	}


	public function addUser(){
		$this->load->model("user_model");
		$userData = $_POST["data"];
		$this->user_model->userInsert($userData["role"],$userData["name"],$userData["username"],$userData["password"]);
	}



}
?>